#include <dirent.h>
#include <limits.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include <algorithm>
#include <fstream>
#include <functional>
#include <iostream>
#include <ranges>
#include <vector>

std::string GetStoragePath() {
  return std::string(getenv("HOME")) + "/.local/share/cl/";
}

bool ExistDirectory(const std::string& path) {
  struct stat s;
  return stat(path.data(), &s) == 0;
}

void CreateDirectoryIfNotExist(const std::string& path) {
  if (!ExistDirectory(path)) {
    mkdir(path.data(), 0777);
  }
}

void CreateFullDirectory(const std::string& path) {
  std::string prefix = "";
  for (auto dir : std::views::split(path, '/')) {
    prefix.append(std::string_view(dir.begin(), dir.end()));
    if (prefix.size() == 0 || prefix.back() != '/') {
      prefix.push_back('/');
    } else {
      continue;
    }

    CreateDirectoryIfNotExist(prefix);
  }
}

// replace / -> _ and _ -> ^_
std::string ConvertPathToString(const std::string_view path) {
  std::string file;
  file.reserve(path.size() + 6);

  for (auto x : path) {
    if (x == '/') {
      file.push_back('_');
    } else if (x == '_') {
      file.append("^_");
    } else {
      file.push_back(x);
    }
  }

  return file;
}

std::string ConvertPathToString(std::string&& path) {
  return ConvertPathToString(static_cast<std::string_view>(path));
}

// replace / -> _ and _ -> ^_
std::string ReconvertPathToString(const std::string_view path) {
  std::string file;
  file.reserve(path.size());

  for (size_t i = 0; i < path.size(); ++i) {
    if (path.compare(i, i + 2, "^_") == 0) {
      file += "_";
      i += 1;
    } else if (path[i] == '_') {
      file += "/";
    } else {
      file += path[i];
    }
  }

  return file;
}

std::string ReconvertPathToString(const std::string& path) {
  return ReconvertPathToString(static_cast<std::string_view>(path));
}

void OpenEditor(const std::string& path) {
  pid_t id = fork();
  if (id == 0) {
    std::string editor{"nvim"};
    std::vector<const char*> v = {editor.data(), path.data(), 0};
    execvp(editor.data(), const_cast<char* const*>(v.data()));
  } else {
    waitpid(id, 0, 0);
  }
}

std::string GetCurrentPath() {
  char cur_path[PATH_MAX];
  getcwd(cur_path, PATH_MAX);

  return std::string(cur_path);
}

std::vector<std::string> GetFilesInDirectory(const std::string& path) {
  DIR* dir = nullptr;
  struct dirent* entry = nullptr;

  dir = opendir(path.data());

  std::vector<std::string> result;
  while ((entry = readdir(dir)) != nullptr) {
    result.emplace_back(entry->d_name);

    if (result.back() == "." || result.back() == "..") {
      result.pop_back();
    }
  };

  closedir(dir);

  return result;
}

const char* ws = " \t\n\r\f\v";

std::string rtrim(std::string s, const char* t = ws) {
  s.erase(s.find_last_not_of(t) + 1);
  return s;
}

std::string ltrim(std::string s, const char* t = ws) {
  s.erase(0, s.find_first_not_of(t));
  return s;
}

std::string trim(std::string s, const char* t = ws) {
  return ltrim(rtrim(s, t), t);
}

void SkipNameCommand(std::ifstream& in, std::string_view command) {
  std::string line;
  while (getline(in, line)) {
    if (line.starts_with(command) && line[command.length()] == ':') {
      return;
    }
  }
}

std::vector<std::string> GetSection(std::ifstream& in) {
  std::string line;
  std::vector<std::string> result;
  while (getline(in, line)) {
    if (line.size() == 0) {
      continue;
    }
    if (!std::isspace(line[0])) {
      return result;
    } else {
      result.push_back(trim(std::move(line)));
    }
  }

  return result;
}

std::vector<std::string> ParseConfig(const std::string& path,
                                     std::string_view command) {
  std::ifstream in(path);

  SkipNameCommand(in, command);

  auto result = GetSection(in);

  in.close();
  return result;
}

void RunCommand(const std::string& cmd) {
  pid_t id = fork();
  if (id == 0) {
    std::string bash = "bash";
    std::vector<const char*> v = {bash.data(), "-c", cmd.data(), 0};
    execvp(bash.data(), const_cast<char* const*>(v.data()));
  } else {
    waitpid(id, 0, 0);
  }
}

std::string ListCommandsToOneCommand(const std::string& path,
                                     const std::vector<std::string>& commands) {
  std::string res = "cd " + path + ";";

  for (auto& i : commands) {
    res += i + ";";
  }

  return res;
}

void PrintListScripts(const std::string& storage) {
  auto files = GetFilesInDirectory(storage);

  for (auto& file : files) {
    std::cout << ReconvertPathToString(file) << "\n";
  }
}

void CreateConfig(const std::string& storage, const std::string& path) {
  OpenEditor(storage + ConvertPathToString(path));
}

std::optional<std::string> GetPathScriptToPath(const std::string& storage,
                                               const std::string& directory) {
  auto files = GetFilesInDirectory(storage);
  std::sort(files.begin(), files.end(),
            [](const std::string& a, const std::string& b) {
              return a.size() > b.size();
            });
  for (const auto& file : files) {
    if (directory.starts_with(file)) {
      return file;
    }
  }

  return {};
}

int main(int argc, char* argv[]) {
  std::string storage = GetStoragePath();
  CreateFullDirectory(storage);

  if (argc <= 1) {
    PrintListScripts(storage);
    return 0;
  }

  if (std::string(argv[1]) == "new") {
    CreateConfig(storage, GetCurrentPath());
  }

  auto current_path = ConvertPathToString(GetCurrentPath());
  auto script_path = GetPathScriptToPath(storage, current_path);

  if (!script_path) {
    if (std::string(argv[1]) == "cfg") {
      CreateConfig(storage, GetCurrentPath());
      return 0;
    }

    std::cout << "config not found" << std::endl;
    return 0;
  }

  if (std::string(argv[1]) == "cfg") {
    OpenEditor(storage + script_path.value());
    return 0;
  }

  auto commands = ParseConfig(storage + script_path.value(), std::string(argv[1]));

  if (commands.size() == 0) {
    std::cout << "command \"" << argv[1] << "\" not found" << std::endl;
    return 0;
  }

  RunCommand(
      ListCommandsToOneCommand(ReconvertPathToString(script_path.value()), commands));
}

